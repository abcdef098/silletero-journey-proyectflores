﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaFlor : MonoBehaviour
{
    private Animator animator;
    private Collider col;
    private Player player;
    private AnimacionVidaFlor florAnim;
    private float VidaCount;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        florAnim = GameObject.FindGameObjectWithTag("FlorUI").GetComponent<AnimacionVidaFlor>();
        animator = GetComponent<Animator>();
        VidaCount = 14.5f; 

    }

    
    void FixedUpdate()
    {
        
        StartCoroutine(Contador());
        if (VidaCount <= 0)
        {
            player.dead();
            
        }

    }

    public void ColisionFlor()
    {
        VidaCount = 14.5f;
        florAnim.ReinicioAnim();
    }

    IEnumerator Contador()
    {
        yield return new WaitForSeconds(0);
        VidaCount -= Time.deltaTime;
    }

}
